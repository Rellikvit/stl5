#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <memory>
#include <string>
#include <cstring>

using namespace std;

void heapify(int arr[], int n, int i)
{
    int largest = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;

    if (l < n && arr[l] > arr[largest])
        largest = l;

    if (r < n && arr[r] > arr[largest])
        largest = r;

    if (largest != i)
    {
        swap(arr[i], arr[largest]);

        heapify(arr, n, largest);
    }
}

void heapSort(int arr[], int n)
{
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    for (int i = n - 1; i >= 0; i--)
    {
        swap(arr[0], arr[i]);

        heapify(arr, i, 0);
    }
}

class Container {

public:
    string m{ "message" };

    Container() 
    {
        cout << "Constructed.\n";
    };

    ~Container()
    {
        cout << "Destroyed.\n";
    };

};

int main()
{
    //Container

    constexpr int n{ 5 };

    alignas(alignof(Container)) char arr[n * sizeof(Container)];

    auto first{ reinterpret_cast<Container*>(arr) };
    auto last{ first + n };

    auto count{ 0 };

    for (auto it{ first }; it != last; ++it) 
    {
        uninitialized_default_construct(it, it + 1);
    }

    //Heap

    int array[] = { 12, 11, 13, 5, 6, 7 };
    int n = sizeof(array) / sizeof(array[0]);

    heapSort(array, n);

    //Permutation

    string s1 = "aba";
    string s2 = "aab";
    bool isOverlaping;
    sort(s1.begin(), s1.end());
    do 
    {
        isOverlaping = s1 == s2;
        if (isOverlaping)
        {
            break;
        }
    } while (next_permutation(s1.begin(), s1.end()));
}